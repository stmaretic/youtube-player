import React, { Component } from "react";
import PropTypes from "prop-types";
import Logo from "./Logo";

const PostedBy = ({ postedBy, image }) => (
  <div className="user-info">
    <Logo image={image} />
    <span className="posted-by">{postedBy}</span>
    <style jsx>
      {`
        .user-info {
          display: flex;
          margin: 30px 0;
          align-items: center;
        }

        .posted-by {
          margin: 0 8px !important;
          font-weight: bold;
          letter-spacing: 0.045rem;
        }
      `}
    </style>
  </div>
);

PostedBy.defaultProps = {
  postedBy: undefined,
  image: undefined
};

PostedBy.propTypes = {
  postedBy: PropTypes.string,
  image: PropTypes.string
};

export default PostedBy;
