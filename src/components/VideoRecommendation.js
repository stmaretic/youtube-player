import React from "react";
import PropTypes from "prop-types";

const VideoRecommendation = ({ data }) => (
  <div className="video-recommendation__container">
    <img
      className="video-recommendation"
      src={`../static/img/${data.thumbnail}`}
    />
    <h3 className="video-recommendation__title">{data.title}</h3>
    <span>smxboy</span>
    <span>100B views</span>
  </div>
);

VideoRecommendation.propTypes = {
  data: PropTypes.object.isRequired
};

export default VideoRecommendation;
