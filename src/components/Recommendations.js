import React from "react";
import VideoRecommendation from "./VideoRecommendation";
import "../styles/Recommendations.css";

const recommendations = [
  {
    id: 1,
    title: "Bokeh",
    thumbnail: "bokeh.jpg"
  },
  {
    id: 2,
    title: "Bokeh",
    thumbnail: "bokeh.jpg"
  },
  {
    id: 3,
    title: "Bokeh",
    thumbnail: "bokeh.jpg"
  }
];

const Recommendations = () => (
  <div className="recommendations__container">
    {recommendations.map(field => (
      <VideoRecommendation key={field.id} data={field} />
    ))}
  </div>
);

export default Recommendations;
