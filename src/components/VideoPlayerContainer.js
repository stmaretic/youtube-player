import React, { Component } from "react";
import VideoPlayer from "./VideoPlayer";
import VideoDescription from "./VideoDescription";

class VideoPlayerContainer extends Component {
  state = {
    video: {
      url: "bokeh.mp4",
      title: "Bokeh",
      description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry.
      Lorem Ipsum has been the industry's standard dummy text ever since the
      1500s, when an unknown printer took a galley of type and scrambled it to
      make a type specimen book. It has survived not only five centuries, but also
      the leap into electronic typesetting, remaining essentially unchanged. It
      was popularised in the 1960s with the release of Letraset sheets containing
      Lorem Ipsum passages, and more recently with desktop publishing software
      like Aldus PageMaker including versions of Lorem Ipsum.`,
      postedBy: "Stefan Maretic"
    }
  };

  render() {
    return (
      <div className="video-player__container">
        <VideoPlayer videoURL={this.state.video.url} />
        <VideoDescription vid={this.state.video} />
        <style jsx>
          {`
            .video-player__container {
              display: inline-block;
              max-width: 50%;
            }
          `}
        </style>
      </div>
    );
  }
}

export default VideoPlayerContainer;
