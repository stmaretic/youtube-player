import React from "react";

const Footer = () => (
  <footer className="footer">
    <ul>
      <li>Lorem ipsum</li>
      <li>Dolor sit amet</li>
      <li>Whatever</li>
    </ul>
    <ul>
      <li>Lorem ipsum</li>
      <li>Dolor sit amet</li>
      <li>Whatever</li>
    </ul>
    <ul>
      <li>Lorem ipsum</li>
      <li>Dolor sit amet</li>
      <li>Whatever</li>
    </ul>
    <style jsx>
      {`
        footer {
          display: flex;
        }

        footer > ul {
          display: inline-block;
          flex: 1 0 auto;
          margin: 60px 0;
          text-align: center;
        }

        footer > ul > li {
          list-style-type: none;
        }
      `}
    </style>
  </footer>
);

export default Footer;
