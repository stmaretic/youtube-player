import React, { Component } from "react";
import axios from "axios";
import User from "./User";

class AdminDashboard extends Component {
  state = {
    user: {}
  };

  componentDidMount() {
    setTimeout(1000, this.getUser());
  }

  componentDidUpdate() {
    console.log(this.state);
  }

  getUser = () => {
    this.setState({
      user: {
        name: "Stefan Maretic",
        username: "smx"
      }
    });
  };

  render() {
    return <User user={this.state.user} />;
  }
}

export default AdminDashboard;
