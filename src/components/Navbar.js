import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import AuthenticationButtons from "./AuthenticationButtons";
import Logout from "./Logout";
import { logoutUser } from "../actions/users";

// import "../styles/Navbar.css";

const Navbar = ({ userAuthed, ...props }) => (
  <nav>
    <ul>
      <li>
        <Link to="/">Home</Link>
      </li>
      <li>
        <Link to="/about">About</Link>
      </li>
      <li>
        <Link to="/contact">Contact</Link>
      </li>
    </ul>
    {userAuthed &&
    userAuthed.status === 200 &&
    userAuthed.statusText === "OK" ? (
      <ul>
        <li>
          <a
            href="#"
            className="button-blue"
            onClick={async () => {
              await props.logoutUser();
              await props.history.push("/login");
            }}
          >
            Logout
          </a>
        </li>
      </ul>
    ) : (
      <ul>
        <li>
          <Link className="button-green" to="/login">
            Login
          </Link>
        </li>
        <li>
          <Link className="button-blue" to="/register">
            Register
          </Link>
        </li>
      </ul>
      // <Logout />
      // <AuthenticationButtons />
    )}
    <style jsx>
      {`
        nav {
          width: 100%;
        }

        nav > ul {
          margin: 0;
          display: inline-block;
        }

        nav > ul > li {
          display: inline-block;
          padding: 35px 20px;
          font-size: 14px;
          letter-spacing: 0.045rem;
          list-style-type: none;
          text-transform: uppercase;
        }

        nav > ul:nth-child(2) > li:nth-last-child(1),
        nav > ul:nth-child(2) > li:nth-last-child(2) {
          padding: 35px 0;
        }

        nav > ul:nth-child(2) > li:nth-last-child(1) {
          padding-right: 20px;
        }

        nav > ul:nth-child(2) {
          float: right;
        }
      `}
    </style>
  </nav>
);

Navbar.defaultProps = {
  userAuthed: {}
};

Navbar.propTypes = {
  userAuthed: PropTypes.object,
  logoutUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  userAuthed: state.users && state.users.user
});

const mapDispatchToProps = {
  logoutUser
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar));
