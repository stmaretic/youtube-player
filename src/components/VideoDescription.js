import React, { Component } from "react";
import PropTypes from "prop-types";
import PostedBy from "./PostedBy";

const VideoDescription = ({ vid }) => (
  <div>
    <h2>{vid.title}</h2>
    <PostedBy postedBy={vid.postedBy} image={vid.image} />
    <p>{vid.description}</p>
  </div>
);

VideoDescription.propTypes = {
  vid: PropTypes.object.isRequired
};

export default VideoDescription;
