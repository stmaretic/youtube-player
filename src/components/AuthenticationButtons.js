import React from "react";
import { Link } from "react-router-dom";

const AuthenticationButtons = props => (
  <ul>
    <li>
      <Link className="button-green" to="/login">
        Login
      </Link>
    </li>
    <li>
      <Link className="button-blue" to="/register">
        Register
      </Link>
    </li>
  </ul>
);

export default AuthenticationButtons;
