import React, { Component } from "react";
import PropTypes from "prop-types";

const InputField = ({
  label,
  type,
  name,
  placeholder,
  input,
  className,
  meta
}) => {
  return (
    <div className="input-container">
      <label htmlFor={name}>{label}</label>
      <input
        id={name}
        type={type}
        name={name}
        placeholder={placeholder}
        className={className}
        {...input}
      />
      {meta.touched &&
        meta.error && <span className="error">{meta.error}</span>}
      <style jsx>
        {`
          div.input-container {
            width: 90%;
            flex: 1 0 100%;
          }

          label {
            display: block;
            margin: 0 auto;
            padding: 4px 0 0 28px;
            margin-bottom: 4px;
          }

          input {
            display: block;
            flex: 1 0 100%;
            width: 90%;
            margin: 0 auto;
            margin-bottom: 8px;
            padding: 10px 12px;
            font-size: 16px;
            border: 1px solid #aaa;
            border-radius: 4px;
          }

          span.error {
            color: red;
          }
        `}
      </style>
    </div>
  );
};

InputField.defaultProps = {
  label: null,
  type: null,
  name: null,
  placeholder: null,
  field: "text",
  className: null
};

InputField.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  field: PropTypes.string,
  className: PropTypes.string,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired
};

export default InputField;
