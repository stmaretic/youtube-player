import React, { Component } from "react";
import PropTypes from "prop-types";

const Logo = ({ image }) => (
  <div className="user-logo" style={image ? { backgroundImage: image } : {}}>
    <style jsx>
      {`
        .user-logo {
          display: inline-block;
          width: 16px;
          height: 16px;
          padding: 16px;
          background-color: #aaa;
          border-radius: 50%;
        }
      `}
    </style>
  </div>
);

Logo.defaultProps = {
  image: undefined
};

Logo.propTypes = {
  image: PropTypes.string
};

export default Logo;
