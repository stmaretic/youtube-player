import React from "react";
import PropTypes from "prop-types";

const User = ({ user }) => (
  <div>
    <h1>User</h1>
    <h1>
      <strong>{user.name}</strong>
    </h1>
    <h3>{user.username}</h3>
  </div>
);

User.propTypes = {
  user: PropTypes.object.isRequired
};

export default User;
