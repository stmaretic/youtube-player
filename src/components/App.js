import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import About from "../pages/About";
import Admin from "../pages/Admin";
import Contact from "../pages/Contact";
import Home from "../pages/Home";
import Login from "../pages/Login";
import Navbar from "./Navbar";
import Register from "../pages/Register";

const App = () => (
  <Router>
    <div className="content">
      <Navbar />
      <div className="container">
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/admin" component={Admin} />
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact} />
      </div>
    </div>
  </Router>
);

export default App;
