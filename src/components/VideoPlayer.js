import React, { Component } from "react";
import PropTypes from "prop-types";
import YouTube from "react-youtube";
import Loading from "../components/Loading";
import VideoDescription from "../components/VideoDescription";

const apiKey = "AIzaSyAQnXWY8iGEAMzmu8X8rWwmoDMn8BGTa78";

class VideoPlayer extends Component {
  onReady = e => {
    e.target.pauseVideo();
  };

  render() {
    const opts = {
      height: "480",
      width: "854",
      playerVars: {
        autoplay: 0
      }
    };

    return <YouTube videoId="htgr3pvBr-I" opts={opts} onReady={this.onReady} />;
  }
}

export default VideoPlayer;
