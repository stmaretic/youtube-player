import { createStore, applyMiddleware, compose } from "redux";
import reduxThunkMiddleware from "redux-thunk";
// import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducers/index";
// import rootSaga from "./actions/sagas";

// const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(reduxThunkMiddleware))
);

// sagaMiddleware.run(rootSaga);

// export const action = (type, payload) => store.dispatch({ type, payload });
// export const action = type => store.dispatch({ type });

export default store;
