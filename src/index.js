import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./createStore";
import App from "./components/App";
import withFooter from "./containers/withFooter";
import "./styles/index.css";
import "./styles/Forms.css";

const Wrapper = () => (
  <Provider store={store}>
    <App />
  </Provider>
);
const WrapperWithFooter = () => withFooter(Wrapper);

ReactDOM.render(<WrapperWithFooter />, document.getElementById("root"));
