import React from "react";
import { shallow } from "enzyme";
import VideoPlayer from "../../components/VideoPlayer";

test("should properly render VideoPlayer component", () => {
  const wrapper = shallow(<VideoPlayer />);
  expect(wrapper).toMatchSnapshot();
});
