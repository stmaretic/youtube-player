import React from "react";
import { shallow } from "enzyme";
import Navbar from "../../components/Navbar";

test("should properly render Navbar component", () => {
  const wrapper = shallow(<Navbar />);
  expect(wrapper).toMatchSnapshot();
});
