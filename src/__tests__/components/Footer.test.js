import React from "react";
import { shallow } from "enzyme";
import Footer from "../../components/Footer";

test("should properly render Footer component", () => {
  const wrapper = shallow(<Footer />);
  expect(wrapper).toMatchSnapshot();
});
