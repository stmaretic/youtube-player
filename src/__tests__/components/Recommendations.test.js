import React from "react";
import { shallow } from "enzyme";
import Recommendations from "../../components/Recommendations";

test("should properly render Recommendations component", () => {
  const wrapper = shallow(<Recommendations />);
  expect(wrapper).toMatchSnapshot();
});
