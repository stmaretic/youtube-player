import React from "react";
import { shallow } from "enzyme";
import VideoRecommendation from "../../components/VideoRecommendation";

const recommendations = [
  {
    id: 1,
    title: "Bokeh",
    thumbnail: "bokeh.jpg"
  },
  {
    id: 2,
    title: "Bokeh",
    thumbnail: "bokeh.jpg"
  },
  {
    id: 3,
    title: "Bokeh",
    thumbnail: "bokeh.jpg"
  }
];

test("should properly render VideoRecommendation component", () => {
  const wrapper = shallow(<VideoRecommendation data={recommendations[0]} />);
  expect(wrapper).toMatchSnapshot();
});
