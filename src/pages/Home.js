import React from "react";

import Recommendations from "../components/Recommendations";
import VideoPlayerContainer from "../components/VideoPlayerContainer";

const Home = () => (
  <div>
    <h1>Home</h1>
    <VideoPlayerContainer />
    <Recommendations />
  </div>
);

export default Home;
