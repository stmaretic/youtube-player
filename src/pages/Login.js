import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";
import axios from "axios";
import InputField from "../components/InputField";
import { fetchUser } from "../actions/users";
import { USER_FETCH_REQUESTED } from "../actions/actionTypes";

class Login extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    fetchUser: PropTypes.func.isRequired
  };

  handleSubmit = async e => {
    await e.preventDefault();
    const form = document.getElementById("login").elements;
    const data = {};
    for (const i in form) {
      if (form[i].value) {
        data[form[i].name] = form[i].value;
      }
    }
    await this.props.fetchUser(data);
    await this.props.history.push("/");
  };

  render() {
    // const { handleSubmit } = this.props;
    return (
      <div className="form-container">
        <form id="login" onSubmit={this.handleSubmit}>
          <h2>Login</h2>
          <Field
            name="email"
            component={InputField}
            type="email"
            label="Email"
            placeholder="Type your email here"
          />
          <Field
            name="password"
            component={InputField}
            type="password"
            label="Password"
            placeholder="Type your password here"
          />
          <button className="button-hover" type="submit">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  fetchUser
};

const LoginWithForm = reduxForm({
  form: "login"
})(Login);

export default withRouter(connect(null, mapDispatchToProps)(LoginWithForm));
