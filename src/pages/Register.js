import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import axios from "axios";
import {
  USER_REGISTER_REQUESTED,
  USER_REGISTER_FAILED,
  USER_REGISTER_SUCCESSFUL
} from "../actions/actionTypes";
import store from "../createStore";
import InputField from "../components/InputField";

class Register extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    requestRegisterUser: PropTypes.func.isRequired
  };

  handleSubmit = async e => {
    e.preventDefault();
    const form = document.getElementById("register").elements;
    const data = {};
    for (const i in form) {
      if (form[i].value) {
        data[form[i].name] = form[i].value;
      }
    }

    const request = axios
      .post("http://localhost:8000/api/register", {
        data
      })
      .then(this.props.requestRegisterUser())
      .then(async res => {
        store.dispatch({ type: USER_REGISTER_SUCCESSFUL, payload: res });
        await this.props.history.push("/login");
      })
      .catch(err => {
        console.error(err.toString());
        store.dispatch({ type: USER_REGISTER_FAILED, payload: err });
      });
  };

  render() {
    return (
      <div className="form-container">
        <form id="register" onSubmit={this.handleSubmit}>
          <h2>Register</h2>
          <Field
            name="firstName"
            component={InputField}
            type="text"
            label="First Name"
            placeholder="First Name"
          />
          <Field
            name="lastName"
            component={InputField}
            type="text"
            label="Last Name"
            placeholder="Last Name"
          />
          <Field
            name="username"
            component={InputField}
            type="text"
            label="Username"
            placeholder="Username"
          />
          <Field
            name="email"
            component={InputField}
            type="email"
            label="Email"
            placeholder="Type your email here"
          />
          <Field
            name="password"
            component={InputField}
            type="password"
            label="Password"
            placeholder="Password should be at least 6 characters"
          />
          {/* <InputField
            name="passwordRepeat"
            label="Repeat Password"
            placeholder="Repeat your password"
            type="password"
          /> */}
          <button className="button-hover" type="submit">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

const RegisterWithForm = reduxForm({
  form: "register"
})(Register);

const mapDispatchToProps = dispatch => ({
  requestRegisterUser: () => dispatch({ type: USER_REGISTER_REQUESTED })
});

export default connect(null, mapDispatchToProps)(withRouter(RegisterWithForm));
