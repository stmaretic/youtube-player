import {
  USER_FETCH_FAILED,
  USER_FETCH_SUCCESSFUL,
  USER_FETCH_REQUESTED,
  USER_REGISTER_REQUESTED,
  USER_REGISTER_FAILED,
  USER_REGISTER_SUCCESSFUL,
  LOGOUT_USER
} from "../actions/actionTypes";

const initialState = {
  user: {}
};

const users = (state = initialState, action) => {
  switch (action.type) {
    case USER_FETCH_REQUESTED:
      return { ...state, user: { loading: true } };
    case USER_FETCH_SUCCESSFUL:
      return { ...state, user: { ...action.payload, loading: false } };
    case USER_FETCH_FAILED:
      return { ...state, user: { loading: false, error: action.payload } };
    case USER_REGISTER_REQUESTED:
      return { ...state, user: { loading: true } };
    case USER_REGISTER_SUCCESSFUL:
      return {
        ...state,
        user: { loading: false, registered: { success: true } }
      };
    case USER_REGISTER_FAILED:
      return {
        ...state,
        user: {
          loading: false,
          registered: { success: false, error: action.payload }
        }
      };
    case LOGOUT_USER:
      return { ...state, user: {} };
    default:
      return state;
  }
};

export default users;
