export const USER_FETCH_REQUESTED = "USER_FETCH_REQUESTED";
export const USER_FETCH_SUCCESSFUL = "USER_FETCH_SUCCESSFUL";
export const USER_FETCH_FAILED = "USER_FETCH_FAILED";
export const USER_REGISTER_REQUESTED = "USER_REGISTER_REQUESTED";
export const USER_REGISTER_SUCCESSFUL = "USER_REGISTER_SUCCESSFUL";
export const USER_REGISTER_FAILED = "USER_REGISTER_FAILED";
export const LOGOUT_USER = "LOGOUT_USER";
