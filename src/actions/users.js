import axios from "axios";
import {
  USER_FETCH_REQUESTED,
  USER_FETCH_SUCCESSFUL,
  USER_FETCH_FAILED,
  LOGOUT_USER
} from "./actionTypes";

export const fetchUser = data => {
  return dispatch => {
    try {
      const request = axios
        .post("http://localhost:8000/api/login", {
          data
        })
        .then(
          dispatch({
            type: USER_FETCH_REQUESTED
          })
        )
        .then(res =>
          dispatch({
            type: USER_FETCH_SUCCESSFUL,
            payload: res
          })
        )
        .catch(err => dispatch({ type: USER_FETCH_FAILED, payload: err }));
    } catch (err) {
      dispatch({
        type: USER_FETCH_FAILED,
        payload: err
      });
    }
  };
};

export const logoutUser = () => {
  return async dispatch => {
    await dispatch({
      type: LOGOUT_USER
    });
  };
};
