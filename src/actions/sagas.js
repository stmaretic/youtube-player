import { all, call, put, takeEvery, takeLatest } from "redux-saga/effects";
import axios from "axios";
import {
  USER_FETCH_SUCCESSFUL,
  USER_FETCH_FAILED,
  USER_FETCH_REQUESTED
} from "./actionTypes";

export function fetchUser(action) {
  const request = axios.post("http://localhost:8000/api/login", {
    data: action.payload
  });
}

function* workerUser() {
  try {
    const response = yield call(fetchUser);
    yield put({ type: USER_FETCH_SUCCESSFUL });
  } catch (err) {
    yield put({ type: USER_FETCH_FAILED, payload: err });
  }
}

function* watcherUsers() {
  yield takeEvery(USER_FETCH_REQUESTED, workerUser);
}

export default function* rootSaga() {
  yield all([watcherUsers()]);
}
