import Radium, { Style } from "radium";

export const styles = {
  footer: {
    display: "flex"
  },

  footerUl: {
    display: "inline-block",
    flex: "1 0 auto",
    margin: "60px 0",
    "text-align": "center"
  },

  footerLiItem: {
    "list-style-type": "none"
  }
};
