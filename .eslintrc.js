module.exports = {
  extends: ["airbnb", "react-app", "plugin:prettier/recommended"],
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error",
    "react/jsx-filename-extension": false,
    "react/forbid-prop-types": false,
    "jsx-a11y/anchor-is-valid": false,
    "jsx-a11y/media-has-caption": false,
    "jsx-a11y/label-has-for": false,
    "jsx-a11y/href-no-hash": false,
    "no-underscore-dangle": 1,
    "arrow-body-style": 0
  }
};
