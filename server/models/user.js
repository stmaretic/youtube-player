const Sequelize = require("sequelize");
const sequelize = require("../db");

const User = sequelize.define(
  "User",
  {
    firstName: Sequelize.STRING,
    lastName: Sequelize.STRING,
    email: { type: Sequelize.STRING, unique: true },
    username: Sequelize.STRING,
    password: Sequelize.STRING
    // timestamps: true
  },
  {}
);
User.associate = function(models) {
  // associations can be defined here
};

module.exports = User;
