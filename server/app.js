const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConnection = require("./db");
const User = require("./models/user");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

const port = process.env.PORT || 8000;

app.get("/api", (req, res) => {
  dbConnection
    .authenticate()
    .then(() => {
      console.log("Connection has been established successfully.");
    })
    .catch(err => {
      console.error("Unable to connect to the database: ", err);
    });
});

app.post("/api/login", async (req, res) => {
  if (req.body) {
    const user = User.findOne({
      where: {
        email: req.body.data.email,
        password: req.body.data.password
      }
    })
      .then(user => {
        res.json(user);
      })
      .catch(err => {
        console.error(err.toString());
      });
  }
});

app.post("/api/register", (req, res) => {
  if (req.body) {
    const user = User.build({
      firstName: req.body.data.firstName,
      lastName: req.body.data.lastName,
      email: req.body.data.email,
      username: req.body.data.username,
      password: req.body.data.password
    })
      .save()
      .then(user => {
        res.json(user);
      })
      .catch(err => {
        console.error(err.toString());
      });
  }
});

app.listen(port, () => console.log("Listening on port " + port));
