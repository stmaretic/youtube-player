const Sequelize = require("sequelize");
const db = "yt_player";
const user = "root";
const password = "root";

const sequelize = new Sequelize(db, user, password, {
  host: "localhost",
  dialect: "mysql",
  define: {
    charset: "utf8",
    collate: "utf8_general_ci",
    timestamps: true
  }
});

module.exports = sequelize;
